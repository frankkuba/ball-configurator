﻿using HTC.UnityPlugin.Vive;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;
using Valve.VR;

public class ConfiguratorController : MonoBehaviour
{

    /// <summary>
    /// Default name of configuration file
    /// </summary>
    private static readonly string DEFAULT_NAME = "configuration.xml";
    /// <summary>
    /// Transform of chest tracker
    /// </summary>
    public Transform chest;
    /// <summary>
    /// Transform of forearm tracker
    /// </summary>
    public Transform foreArm;
    /// <summary>
    /// Prefab of point
    /// </summary>
    public GameObject pointPrefab;
    /// <summary>
    /// GameObject as parent of all points
    /// </summary>
    public GameObject pointsParent;

    /// <summary>
    /// List of points
    /// </summary>
    private List<GameObject> points = new List<GameObject>();

    /// <summary>
    /// Name of saved file
    /// </summary>
    public string fileName;
    /// <summary>
    /// trigger button of controller
    /// </summary>
    private EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;

    XmlTextWriter writer;
    FileStream stream;

    /// <summary>
    /// number of balls
    /// </summary>
    int i;

    
    void Start()
    {
       if(fileName == null)
        {
            fileName = DEFAULT_NAME;
        }
       //initialization of xml writer
        stream = new FileStream(fileName, FileMode.Create);
        writer = new XmlTextWriter(stream, null);
        writer.Formatting = Formatting.Indented;
        writer.Indentation = 4;
        writer.WriteStartDocument();
        writer.WriteStartElement("configuration");
        i = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(ViveInput.GetPressDown(HandRole.RightHand, ControllerButton.Trigger))
        {
            print("new point, index: " + i);
            print("forearm: " + foreArm.position);
            print("chest: " + chest.position);

            Vector3 vec = foreArm.position - chest.position;
            writePoint(vec);
            GameObject point = Instantiate(pointPrefab);             
            point.transform.position = foreArm.position;
                
            i++;
        }
        if(ViveInput.GetPressDown(HandRole.RightHand, ControllerButton.Menu) || Input.GetKeyDown(KeyCode.Escape))
        {
            print("configuration end");
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
              Application.Quit();
        #endif


        }


    }

    /// <summary>
    /// Method to save configuration file on exit
    /// </summary>
    private void OnDestroy()
    {
        writer.WriteEndElement();
        writer.WriteEndDocument();
        writer.Flush();
        writer.Close();
        



    }
    /// <summary>
    /// Writes position of ball in config file
    /// </summary>
    /// <param name="vector">position of ball</param>
    private void writePoint(Vector3 vector)
    {
        writer.WriteStartElement("point");
        writer.WriteAttributeString("index", i.ToString());     
        writer.WriteElementString("x", vector.x.ToString());
        writer.WriteElementString("y", vector.y.ToString());
        writer.WriteElementString("z", vector.z.ToString());

        writer.WriteEndElement();
    }


}
